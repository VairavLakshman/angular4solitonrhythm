const express = require('express');
const router = express.Router();
const config = require('../../config/config.js');
const GoogleSpreadsheet = require('google-spreadsheet');
const async = require('async');

const doc = new GoogleSpreadsheet(config.google.doc); //Accessing the gsheet to get the data

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    sheet1Data: [],
    sheet2Data: [],
    projectNames: [],
    message: null
};

// Get users
router.get('/users', (req, res) => {
  var fields = []; //Array to store the seperate rows data
  var projectNames = [];
  var sheet1Data = []; //Array to store the firstSheet data
  var sheet2Data = []; //Array to store the secondSheet data

  async.series([
    function setAuth(step) {
      var credits = require('../../config/SRVCalc-c42052b52e0c.json');
      doc.useServiceAccountAuth(credits, step);
    },
    function getInfoAndWorksheets(step) {
      doc.getInfo(function(err, info) {
        console.log('Loaded doc: '+info.title+' by '+info.author.email);
        sheet = info.worksheets[0];
        console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
        secondSheet = info.worksheets[1];
        console.log('sheet 2: '+secondSheet.title+' '+secondSheet.rowCount+'x'+secondSheet.colCount);
        step();
      });
    },
    //Function to go through each rows of the spreadsheet and store it in the array
    function workingWithRows(step) {
        /*jslint undef:true*/
        sheet.getRows({
            offset: config.google.offset,
            limit: config.google.limit,
            orderby: 'col2'
        }, function (err, rows) {
            var i;
            for (i = 0; i < rows.length; i += 1) {
              fields.push(rows[i].projectname);
              projectNames.push(rows[i].projectname)
              fields.push(rows[i].member);
              fields.push(rows[i].weekofdate);
              fields.push(rows[i].status);
              fields.push(rows[i].totalhours);
              sheet1Data.push(fields);
              fields = [];
            }
            projectNames = projectNames.filter(function(item, pos){
                              return projectNames.indexOf(item)== pos;
                           });
            response.projectNames = projectNames;
            response.sheet1Data = sheet1Data;
            console.log(projectNames.length);
            res.json(response);
        });
        secondSheet.getRows({
            offset: config.google.offset,
            limit: config.google.limit,
            orderby: 'col2'
        }, function (err, rows) {
            var i;
            for (i = 0; i < rows.length; i += 1) {
              sheet2Data.push(rows[i].rhythmreportingname);
            }
            response.sheet2Data = sheet2Data;
        });
        step();
    }
  ], function(err){
      if( err ) {
        console.log('Error: '+err);
      }
  });
});

module.exports = router;
