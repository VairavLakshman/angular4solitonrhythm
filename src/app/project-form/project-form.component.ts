import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { DataService } from '../data.service';
import { WeekPipe } from '../week.pipe';
@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.css'],
  providers: [ WeekPipe ]
})
export class ProjectFormComponent implements OnInit {

  projectNames: any;
  rhythmreportingnames: any;
  data: any;
  selectedProject: string;
  selectedWeek: string;
  reportingweeks: Array<any> = [];
  weekNumber: any;
  fileteredList: Array<any> = [];
  finalData: Object = {};
  dataToDisplay: Array<any> = [];
  count: number = 0;
  employeeCount: number = 0;
  isFormSubmitted: boolean = false;
  employees: Array<any> = [];
  selectedWeekName: any;

  constructor(private dataService: DataService, private weekPipe: WeekPipe) {}

  ngOnInit() {
    let date = new Date();
    this.weekNumber = this.weekPipe.transform(date)

    this.reportingweeks.push("All")
    this.reportingweeks.push("Week " + (this.weekNumber - 1).toString())
    this.reportingweeks.push("Week " + (this.weekNumber - 2).toString())

    this.dataService.getUsers()
      .subscribe(res => {
        this.projectNames = res.projectNames
        this.rhythmreportingnames = res.sheet2Data
        this.data = res.sheet1Data
      });
  }

  onFormSubmit(form: NgForm) {
    this.isFormSubmitted = true;
    let selectedGroup = form.controls['selectGroup'].value;
    let selectedProjects = form.controls['selectProject'].value;
    this.selectedWeekName = form.controls['selectWeek'].value;
    this.fileteredList = [];
    this.finalData = {};
    for (let name of selectedProjects) {
      for (let rows in this.data) {
        if (this.selectedWeekName === "All") {
          if (this.data[rows][0] === name) {
            this.fileteredList.push(this.data[rows])
          }
        } else {
          if (this.data[rows][0] === name && this.data[rows][2] === this.selectedWeekName) {
            this.fileteredList.push(this.data[rows])
          }
        }
      }
    }

    for (let rows of this.fileteredList) {
      if(this.finalData[rows[1]] === undefined) {
        this.finalData[rows[1]] = [];
      }
      this.finalData[rows[1]].push(rows);
    }

    for (let key of Object.keys(this.finalData)) {
      this.dataToDisplay[this.count] = [];
      this.dataToDisplay[this.count].push(this.finalData[key]);
      this.count += 1;
    }

    for(let rows of this.dataToDisplay) {
      for(let row of rows) {
        this.employees[this.employeeCount] = [];
        let hourSpent: number = 0;
        let billable: number = 0;
        let name: string = "";
        let planned: number = 40;
        for(let innerRow of row) {
          hourSpent += parseFloat(innerRow[4]);
          if (innerRow[3] === "Billable") {
            billable += parseFloat(innerRow[4]);
          }
          name = innerRow[1];
        }
        this.employees[this.employeeCount].push(name);
        this.employees[this.employeeCount].push(hourSpent.toFixed(2));
        this.employees[this.employeeCount].push(planned.toFixed(2));
        this.employees[this.employeeCount].push(billable.toFixed(2));
        this.employees[this.employeeCount].push(((billable / hourSpent) * 100).toFixed(2));
        this.employees[this.employeeCount].push(((hourSpent / planned) * 100).toFixed(2));
        this.employeeCount += 1;
      }
    }

  }

  setNewGroup(value: string): void {
    this.selectedProject = value;
    console.log("SetNewGroup")
    console.log(this.selectedProject)
  }

  setNewWeek(value: string): void {
    this.selectedWeek = value;
    console.log("SetNewWeek")
    console.log(this.selectedWeek)
  }

}
