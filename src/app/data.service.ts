import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  constructor(private _http: Http) { }

  getUsers(): Observable<any>{
    return this._http.get("/api/users")
      .map(result=> {
        return result.json()
      });
  }

}
