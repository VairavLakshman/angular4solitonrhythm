import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { DataService } from './data.service';
import { ProjectFormComponent } from './project-form/project-form.component';
import { WeekPipe } from './week.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ProjectFormComponent,
    WeekPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
