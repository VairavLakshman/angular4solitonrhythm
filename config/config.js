/*jslint undef:true*/
// i = 1, run the app in localhost
// i = 2, run tha app in devsolitonrhythmviewer.azurewebsites.net
// i = 3, run the app in solitonrhythmviewer.azurewebsites.net
var i = 1, id;
if (i === 1) {
    //This will be in action when the app is running in local host
    id = {
        google: {
            //Id to authenticate the app
            clientId: "632215015349-csmuarqken9hipkn2401ummteuho03im.apps.googleusercontent.com",
            clientSecret: "gnKn2DwSYKk5jIZJzMYk8hWm",
            //Redirection after successful authentication
            callbackURL: "http://localhost:3000/auth/google/callback",
            passReqToCallback: true,
            //Connection to Heroku-postgreSQL DBaaS-Black(Color Code to differentiate the DB)
            connectionString: "postgres://jvrsilpqfzsybv:R0A415DN37pZj5x5r6dZH757Xq@ec2-54-235-119-29.compute-1.amazonaws.com:5432/d78qb79s7pnqej",
            //Connecting to the google sheet Testing-Soliton Rhythm
            doc: '1MiH8paZvBxZD3ejCtfc9vh_FrVjZ5Ao40nbNoQIyFTY',
            //Setting the rowlimit and offset of the spreadsheet
            limit: 3000,
            offset: 1,
            //Boolean to perform rhythm table updation
            updateDBRhythmData: false
        }
    };
} else if (i === 2) {
    //This will be in action when the app is running in beta version(devsolitonrhythmviewer)
    id = {
        google: {
            //Id to authenticate the app
            clientId: "903209434467-rddm8fp83lbhfqefnb0ob05c5ocghu0t.apps.googleusercontent.com",
            clientSecret: "LTGimPoId4yj05s0RWHcZynf",
            //Redirection after successful authentication
            callbackURL: "http://devsolitonrhythmviewer.azurewebsites.net/auth/google/callback",
            passReqToCallback: true,
            //Connection to Heroku-postgreSQL DBaaS-Blue(Color Code to differentiate the DB)
            connectionString: "postgres://hygpsiufqibygz:3yeC4AzJHbsHo_nGkY55TcQM-V@ec2-54-235-119-29.compute-1.amazonaws.com:5432/ddhcte5g4i5k4r",
            //Connecting to the google sheet Testing-Soliton Rhythm
            doc: '1MiH8paZvBxZD3ejCtfc9vh_FrVjZ5Ao40nbNoQIyFTY',
            //Setting the rowlimit and offset of the spreadsheet
            limit: 3000,
            offset: 1,
            //Boolean to perform rhythm table updation
            updateDBRhythmData: false
        }
    };
} else {
    //This will be in action when the app is running in original version(solitonrhythmviewer)
    id = {
        google: {
            //Id to authenticate the app
            clientId: "911182312596-akn3nmksdahjada24fatgou7trls2k55.apps.googleusercontent.com",
            clientSecret: "aiEWelkLseL45ROZmNN__m5-",
            //Redirection after successful authentication
            callbackURL: "http://solitonrhythmviewer.azurewebsites.net/auth/google/callback",
            passReqToCallback: true,
            //Connection to Heroku-postgreSQL DBaaS-Brown(Color Code to differentiate the DB)
            connectionString: "postgres://eelldqbkquxazx:BnD9xDbI6TPMogrZrnhWVFIQaK@ec2-54-235-119-29.compute-1.amazonaws.com:5432/dadtsesc4s7b1l",
            //Connecting to the google sheet Testing-Soliton Rhythm
            doc: '1MiH8paZvBxZD3ejCtfc9vh_FrVjZ5Ao40nbNoQIyFTY',
            //Setting the rowlimit and offset of the spreadsheet
            limit: 3000,
            offset: 1,
            //Boolean to perform rhythm table updation
            updateDBRhythmData: false
        }
    };
}

module.exports = id; //Exporting the appropriate ID to the server file
